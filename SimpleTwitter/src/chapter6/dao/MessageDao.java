package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import chapter6.beans.Message;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {
	public void insert(Connection connection, Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("INSERT INTO messages ( ");
			sb.append(" user_id, ");
			sb.append(" text, ");
			sb.append(" created_date, ");
			sb.append(" updated_date ");
			sb.append(") VALUES ( ");
			sb.append(" ?, "); // user_id
			sb.append(" ?, "); // text
			sb.append(" CURRENT_TIMESTAMP, "); // created_date
			sb.append(" CURRENT_TIMESTAMP "); // updated_date
			sb.append(")");

			ps = connection.prepareStatement(sb.toString());

			ps.setInt(1, message.getUserId());
			ps.setString(2, message.getText());

			ps.executeUpdate();

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);

		}finally {
			close(ps);
		}
	}

}
