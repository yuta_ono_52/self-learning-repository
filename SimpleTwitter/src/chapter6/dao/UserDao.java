package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class UserDao {
	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("INSERT INTO users ( ");
			sb.append(" account, ");
			sb.append(" name, ");
			sb.append(" email, ");
			sb.append(" password, ");
			sb.append(" description, ");
			sb.append(" created_date, ");
			sb.append(" updated_date ");
			sb.append(") VALUES ( ");
			sb.append(" ?, "); // account
			sb.append(" ?, "); // name
			sb.append(" ?, "); // email
			sb.append(" ?, "); // password
			sb.append(" ?, "); // description
			sb.append(" CURRENT_TIMESTAMP, "); // created_date
			sb.append(" CURRENT_TIMESTAMP "); // updated_date
			sb.append(")");

			ps = connection.prepareStatement(sb.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getName());
			ps.setString(3, user.getEmail());
			ps.setString(4, user.getPassword());
			ps.setString(5, user.getDescription());

			ps.executeUpdate();

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	public User select(Connection connection, String accountOrEmail, String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * from users WHERE (account = ? OR email = ?) AND password = ?";

			ps = connection.prepareStatement(sql);

			ps.setString(1, accountOrEmail);
			ps.setString(2, accountOrEmail);
			ps.setString(3, password);

			ResultSet rs = ps.executeQuery();

			List<User> users = toUsers(rs);
			if(users.isEmpty()) {
				return null;
			}else if(2 <= users.size()) {
				throw new IllegalStateException("ユーザーが重複しています");
			}else {
				return users.get(0);
			}
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	private List<User> toUsers(ResultSet rs) throws SQLException{
		List<User> users = new ArrayList<User>();
		try {
			while(rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setAccount(rs.getString("account"));
				user.setName(rs.getString("name"));
				user.setEmail(rs.getString("email"));
				user.setPassword(rs.getString("password"));
				user.setDescription(rs.getString("description"));
				user.setCreatedDate(rs.getTimestamp("created_date"));
				user.setUpdatedDate(rs.getTimestamp("updated_date"));
				users.add(user);
			}
			return users;
		}finally {
			close(rs);
		}
	}

	public User select(Connection connection, int userId) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);

			ps.setInt(1, userId);

			ResultSet rs = ps.executeQuery();

			List<User> users = toUsers(rs);

			if (users.isEmpty()) {
				return null;

			} else if (2 <= users.size()) {
				throw new IllegalStateException("ユーザーが重複しています");

			} else {
				return users.get(0);
			}

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {

		}
	}

	public void update(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("UPDATE users SET ");
			sb.append(" account = ?, ");
			sb.append(" name = ?, ");
			sb.append(" email = ?, ");

			if(!StringUtils.isEmpty(user.getPassword())) {
				sb.append(" password = ?, ");
			}

			sb.append(" description = ?, ");
			sb.append(" updated_date = CURRENT_TIMESTAMP ");
			sb.append("WHERE id = ?");

			ps = connection.prepareStatement(sb.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getName());
			ps.setString(3, user.getEmail());
			if(!StringUtils.isEmpty(user.getPassword())) {
				ps.setString(4, user.getPassword());
				ps.setString(5, user.getDescription());
				ps.setInt(6, user.getId());
			}else {
				ps.setString(4, user.getDescription());
				ps.setInt(5, user.getId());
			}

			int count = ps.executeUpdate();

			if(count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}

	public User select(Connection connection, String account) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE account = ?";

			ps = connection.prepareStatement(sql);

			ps.setString(1, account);

			ResultSet rs = ps.executeQuery();

			List<User> users = toUsers(rs);
			if (users.isEmpty()) {
				return null;

			} else if (2 <= users.size()) {
				throw new IllegalStateException("ユーザーが重複しています");

			} else {
				return users.get(0);
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}
}
