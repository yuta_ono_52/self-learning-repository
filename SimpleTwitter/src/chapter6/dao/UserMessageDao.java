package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserMessage;
import chapter6.exception.SQLRuntimeException;

public class UserMessageDao {
	public List<UserMessage> select(Connection connection, int num){

		PreparedStatement ps = null;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT ");
			sb.append(" messages.id as id, ");
			sb.append(" messages.text as text, ");
			sb.append(" messages.user_id as user_id, ");
			sb.append(" users.account as account, ");
			sb.append(" users.name as name, ");
			sb.append(" messages.created_date as created_date ");
			sb.append("FROM messages ");
			sb.append("INNER JOIN users ");
			sb.append("ON messages.user_id = users.id ");
			sb.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sb.toString());

			ResultSet rs = ps.executeQuery();

			List<UserMessage> messages = toUserMessages(rs);
			return messages;
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);

		}finally {
			close(ps);
		}
	}

	//応用編
	public List<UserMessage> select(Connection connection, int id, int num){

		PreparedStatement ps = null;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT ");
			sb.append(" messages.id as id, ");
			sb.append(" messages.text as text, ");
			sb.append(" messages.user_id as user_id, ");
			sb.append(" users.account as account, ");
			sb.append(" users.name as name, ");
			sb.append(" messages.created_date as created_date ");
			sb.append("FROM messages ");
			sb.append("INNER JOIN users ");
			sb.append("WHERE users.id = ? ");
			sb.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sb.toString());

			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			List<UserMessage> messages = toUserMessages(rs);
			return messages;
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);

		}finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException{

		List<UserMessage> messages = new ArrayList<UserMessage>();
		try {
			while(rs.next()){
				UserMessage message = new UserMessage();
				message.setId(rs.getInt("id"));
				message.setText(rs.getString("text"));
				message.setUserId(rs.getInt("user_id"));
				message.setAccount(rs.getString("account"));
				message.setName(rs.getString("name"));
				message.setCreatedDate(rs.getTimestamp("created_date"));

				messages.add(message);
			}
			return messages;

		}finally {
			close(rs);
		}
	}
}
