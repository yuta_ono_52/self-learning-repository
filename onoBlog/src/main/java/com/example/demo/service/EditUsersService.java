package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.EditUsers;
import com.example.demo.repository.EditUsersRepository;
import com.example.demo.utils.CipherUtil;

@Service
public class EditUsersService {
	@Autowired
	EditUsersRepository editUsersRepository;

	public Boolean findAll(EditUsers editUsers){

		List<EditUsers> usersList = findAllUser();

		String encPassword = CipherUtil.encrypt(editUsers.getPassword());
		editUsers.setPassword(encPassword);

		for(EditUsers user : usersList) {
			if(editUsers.getEmail().equals(user.getEmail())) {
				if(editUsers.getPassword().equals(user.getPassword())) {
					return true;
				}
			}
		}
		return false;
	}

	public Boolean saveEditUser(EditUsers editUsers) {
		//内部メソッドを呼び出し、ユーザー情報を全取得
		List<EditUsers> usersList = findAllUser();

		//取得した全ユーザー情報と、登録予定のユーザー情報のメールアドレスを判定
		for(EditUsers user : usersList) {
			if(user.getEmail().equals(editUsers.getEmail())) {
				return false;
			}
		}

		//パスワードの暗号化
		String encPassword = CipherUtil.encrypt(editUsers.getPassword());
		editUsers.setPassword(encPassword);

		//メールアドレスの重複が無ければ登録処理
		editUsersRepository.save(editUsers);
		return true;
	}

	//登録管理ユーザーを全取得
	public List<EditUsers> findAllUser(){
		return editUsersRepository.findAll();
	}

	//指定ユーザー情報の取得
	public Optional<EditUsers> findBy(int id){
		return  editUsersRepository.findById(id);
	}


	//ユーザー編集（パスワード変更なし）
	public Boolean noPassSave(EditUsers editUsers) {
		//内部メソッドを呼び出し、ユーザー情報を全取得
		List<EditUsers> usersList = findAllUser();

		//取得した全ユーザー情報と、登録予定のユーザー情報のメールアドレスを判定
		for(EditUsers user : usersList) {
			if(user.getEmail().equals(editUsers.getEmail())
					&& (!(user.getId() == editUsers.getId()))) {
				return false;
			}
		}
		editUsersRepository.save(editUsers);
		return true;
	}

	//ユーザー編集（パスワード変更あり）
	public Boolean passSave(EditUsers editUsers) {
		//内部メソッドを呼び出し、ユーザー情報を全取得
		List<EditUsers> usersList = findAllUser();

		//取得した全ユーザー情報と、登録予定のユーザー情報のメールアドレスを判定
		for(EditUsers user : usersList) {
			if(user.getEmail().equals(editUsers.getEmail())
					&& (!(user.getId() == editUsers.getId()))) {
				return false;
			}
		}

		//パスワードの暗号化
		String encPassword = CipherUtil.encrypt(editUsers.getPassword());
		editUsers.setPassword(encPassword);

		//メールアドレスの重複が無ければ登録処理
		editUsersRepository.save(editUsers);
		return true;
	}

}
