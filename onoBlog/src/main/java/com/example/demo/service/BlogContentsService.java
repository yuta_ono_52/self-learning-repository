package com.example.demo.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.BlogContents;
import com.example.demo.repository.BlogContentsCatgRepository;
import com.example.demo.repository.BlogContentsRepository;

@Service
public class BlogContentsService {

	@Autowired
	BlogContentsRepository blogContentsRepository;

	@Autowired
	BlogContentsCatgRepository blogContentsCatgRepository;

	//
	public Optional<BlogContents> findBy(int id) {
		return blogContentsRepository.findById(id);
	}

	//カテゴリ別ブログタイトル取得
	public List<BlogContents> findByCategory_id(String category_id){
		List<BlogContents> list = blogContentsCatgRepository.findAll();

		//カテゴリが該当するブログを格納するリスト
		List<BlogContents> titleList = new ArrayList<BlogContents>();

		//カテゴリの判定
		for(BlogContents bc : list) {
			if(bc.getCategory_id().equals(category_id)) {
				titleList.add(bc);
			}
		}
		return titleList;
	}

	//ブログ記事新規登録
	public Boolean saveNewBlog(BlogContents blogContents) {

		//現在時間を取得、Stringに変換
		LocalDateTime nowDateTime = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		String dateTime = nowDateTime.format(dtf);

		blogContents.setCreate_datetime(dateTime);
		blogContents.setUpdate_datetime(dateTime);

		BlogContents resultContents = new BlogContents();
		resultContents = blogContentsRepository.save(blogContents);

		if(resultContents == null) {
			return false;
		}
		return true;
	}

	//管理画面ブログ一覧
	public List<BlogContents> findAll() {
		List<BlogContents> list = new ArrayList<BlogContents>();
		list = blogContentsRepository.findAll();
		return list;
	}

	//ブログ記事の編集
	public Boolean saveEditBlog(BlogContents bc) {

		LocalDateTime nowDateTime = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		String dateTime = nowDateTime.format(dtf);
		bc.setUpdate_datetime(dateTime);

		BlogContents resultSave = blogContentsRepository.save(bc);

		if(resultSave == null) {
			return false;
		}

		return true;
	}

}
