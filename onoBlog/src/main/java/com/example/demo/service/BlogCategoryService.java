package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.BlogCategory;
import com.example.demo.repository.BlogCategoryRepository;

@Service
public class BlogCategoryService {

	@Autowired
	BlogCategoryRepository blogCategoryRepository;

	public List<BlogCategory> findAll() {
		return blogCategoryRepository.findAll();
	}

	public Optional<BlogCategory> findBy(int categoryId){


		return blogCategoryRepository.findById(categoryId);
	}
}
