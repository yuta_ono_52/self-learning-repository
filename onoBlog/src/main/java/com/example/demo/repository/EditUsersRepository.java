package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.EditUsers;

public interface EditUsersRepository extends JpaRepository<EditUsers, Integer> {

}
