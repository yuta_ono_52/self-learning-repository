package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.BlogContents;

@Repository
public interface BlogContentsRepository extends JpaRepository<BlogContents, Integer> {

//	public List<BlogContents> findAllByOrderByCreate_datetime();
}
