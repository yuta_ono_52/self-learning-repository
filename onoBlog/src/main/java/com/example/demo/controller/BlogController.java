package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.BlogCategory;
import com.example.demo.entity.BlogContents;
import com.example.demo.entity.EditUsers;
import com.example.demo.service.BlogCategoryService;
import com.example.demo.service.BlogContentsService;
import com.example.demo.service.EditUsersService;

@Controller
public class BlogController {

	@Autowired
	BlogContentsService blogContentsService;

	@Autowired
	BlogCategoryService blogCategoryService;

	@Autowired
	EditUsersService editUsersService;

	@Autowired
	HttpSession session;

	private final String nUserValidFlg = "0";

	private final String eUserValidFlg = "1";

	//top画面表示
	@GetMapping
	public ModelAndView top() {

		ModelAndView mav = new ModelAndView();

		//カテゴリ全件取得
		List<BlogCategory> categoryData = blogCategoryService.findAll();

		//画面遷移先を指定
		mav.setViewName("/top");

		//カテゴリを保管
		mav.addObject("categoryData", categoryData);
		return mav;
	}

	//ログイン画面表示
	@GetMapping("/login")
	public ModelAndView login() {

		ModelAndView mav = new ModelAndView();

		//セッションのログインユーザ情報を取得
		EditUsers eUser = (EditUsers) session.getAttribute("loginUser");

		//ログインをすでに行っている場合
		if(eUser != null) {
			//ログイン画面に遷移せずに管理ユーザー専用設定画面に遷移する
			mav.setViewName("redirect:/edit");
		}else {
			//その他ログイン画面に遷移
			EditUsers editUsers = new EditUsers();
			mav.addObject("formModel", editUsers);
			mav.setViewName("/login");
		}
		return mav;
	}

	//ログイン処理
	@PostMapping("/login")
	public ModelAndView loginAuth(@ModelAttribute("formModel") EditUsers user) {

		ModelAndView mav = new ModelAndView();

		//入力ユーザ情報の照会
		Boolean authResult = editUsersService.findAll(user);

		if(authResult) {

			mav.setViewName("redirect:/edit");

			session.setAttribute("loginUser", user);
		}else {
			mav.addObject("alert","メールアドレスまたはパスワードが間違っています");
			mav.addObject("formModel", user);
			mav.setViewName("/login");
		}
		return mav;
	}


	//ブログ一覧画面表示
	@GetMapping("/all_blogtitle/{category_id}")
	public ModelAndView allBlogTitle(@PathVariable String category_id) {

		ModelAndView mav = new ModelAndView();

		List<BlogContents> titleList = blogContentsService.findByCategory_id(category_id);

		mav.setViewName("/all_blogtitle");
		mav.addObject("titleList",titleList);
		return mav;
	}

	//ブログ内容表示画面
	@GetMapping("/blog/{id}")
	public ModelAndView blog(@PathVariable int id) {

		ModelAndView mav = new ModelAndView();

		//ブログ内容を取得
		Optional<BlogContents> blogContents = blogContentsService.findBy(id);
		String title = blogContents.get().getTitle();
		String article = blogContents.get().getArticle();

		//画面遷移先を指定
		mav.setViewName("/blog");

		//ブログ内容を保管
		mav.addObject("title", title);
		mav.addObject("article", article);
		return mav;
	}

	//管理者設定画面表示
	@GetMapping("/edit")
	public ModelAndView edit() {
		//セッションのログインユーザ情報を取得
		EditUsers eUser = (EditUsers) session.getAttribute("loginUser");

		//セッション情報にログインユーザがない場合、ログイン画面にリダイレクト
		if(eUser == null) {
			return new ModelAndView("redirect:/login");
		}

		ModelAndView mav = new ModelAndView();
		//画面遷移先を指定
		mav.setViewName("/edit");
		return mav;
	}

	//管理ユーザ新規登録画面
	@GetMapping("/edit_newuser")
	public ModelAndView editNewUser() {
		//セッションのログインユーザ情報を取得
		EditUsers eUser = (EditUsers) session.getAttribute("loginUser");

		//セッション情報にログインユーザがない場合、ログイン画面にリダイレクト
		if(eUser == null) {
			return new ModelAndView("redirect:/login");
		}

		ModelAndView mav = new ModelAndView();

		//画面遷移先を指定
		mav.setViewName("/edit_newuser");
		EditUsers editUsers = new EditUsers();
		mav.addObject("formModel",editUsers);
		return mav;
	}

	//管理ユーザー新規登録処理
	@PostMapping("edit_newuser")
	public ModelAndView editNewUser(@ModelAttribute("formModel") EditUsers editUsers) {

		ModelAndView mav = new ModelAndView();

		//ユーザー情報バリデーションチェック
		List<String> errorMessages = new ArrayList<String>();
		Boolean valid = validUser(editUsers, errorMessages, nUserValidFlg);

		//エラーメッセージがあった場合
		if(!valid) {
			mav.addObject("alertList", errorMessages);
			return mav;
		}

		Boolean registResult = editUsersService.saveEditUser(editUsers);
		if(!registResult) {
			mav.addObject("alert","このメールアドレスはすでに登録されています");
			return mav;
		}
		mav.setViewName("/regist_success");
		return mav;
	}

	//ブログ記事一覧画面表示
	@GetMapping("/edit_allblog")
	public ModelAndView editAllBlog() {

		//セッションのログインユーザ情報を取得
		EditUsers eUser = (EditUsers) session.getAttribute("loginUser");

		//セッション情報にログインユーザがない場合、ログイン画面にリダイレクト
		if(eUser == null) {
			return new ModelAndView("redirect:/login");
		}

		ModelAndView mav = new ModelAndView();

		//ブログ情報全取得
		List<BlogContents> list = blogContentsService.findAll();

		mav.addObject("blogList", list);
		mav.setViewName("/edit_allblog");
		return mav;
	}

	//ブログ記事新規作成画面表示
	@GetMapping("/create_blog")
	public ModelAndView createBlog() {

		//セッションのログインユーザ情報を取得
		EditUsers eUser = (EditUsers) session.getAttribute("loginUser");

		//セッション情報にログインユーザがない場合、ログイン画面にリダイレクト
		if(eUser == null) {
			return new ModelAndView("redirect:/login");
		}

		ModelAndView mav = new ModelAndView();

		BlogContents blog = new BlogContents();
		mav.addObject("formModel", blog);

		List<BlogCategory> catgList = blogCategoryService.findAll();
		mav.addObject("categoryList",catgList);
		mav.setViewName("/create_blog");
		return mav;
	}

	//新規ブログ記事登録処理
	@PostMapping("/create_blog")
	public ModelAndView createBlog(@ModelAttribute("formModel") BlogContents blogContents) {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();
		Boolean valid = validBlog(blogContents,errorMessages);

		if(!valid) {
			mav.addObject("errorMessages", errorMessages);
			List<BlogCategory> catgList = blogCategoryService.findAll();
			mav.addObject("categoryList",catgList);
			return mav;
		}

		Boolean result = blogContentsService.saveNewBlog(blogContents);

		if(!result) {
			mav.addObject("formModel",blogContents);
			mav.addObject("alertDB","サーバーエラーのため登録できませんでした。管理者へ報告してください");
		}
		List<BlogContents> list = blogContentsService.findAll();
		mav.addObject("blogList", list);
		mav.setViewName("redirect:/edit_allblog");
		return mav;
	}

	//ブログ編集画面表示
	@GetMapping("/edit_blog/{id}")
	public ModelAndView editBlog(@PathVariable int id) {

		//セッションのログインユーザ情報を取得
		EditUsers eUser = (EditUsers) session.getAttribute("loginUser");

		//セッション情報にログインユーザがない場合、ログイン画面にリダイレクト
		if(eUser == null) {
			return new ModelAndView("redirect:/login");
		}

		ModelAndView mav = new ModelAndView();

		Optional<BlogContents> blogContents = blogContentsService.findBy(id);
		String title = blogContents.get().getTitle();
		String article = blogContents.get().getArticle();
		String catgId = blogContents.get().getCategory_id();
		String date = blogContents.get().getCreate_datetime();

		mav.addObject("id", id);
		mav.addObject("category_id",catgId);
		mav.addObject("title", title);
		mav.addObject("article", article);
		mav.addObject("create_datetime", date);
		mav.setViewName("/edit_blog");
		return mav;
	}

	//ブログ編集後、登録処理
	@PostMapping("/edit_blog")
	public ModelAndView editBlog(@ModelAttribute("formModel") BlogContents blogContents) {

		ModelAndView mav = new ModelAndView();

		List<String> errorMessages = new ArrayList<String>();
		Boolean valid = validBlog(blogContents,errorMessages);

		if(!valid) {
			mav.addObject("id", blogContents.getId());
			mav.addObject("category_id",blogContents.getCategory_id());
			mav.addObject("title", blogContents.getTitle());
			mav.addObject("article", blogContents.getArticle());
			mav.addObject("create_datetime", blogContents.getCreate_datetime());

			mav.addObject("errorMessages", errorMessages);
			return mav;
		}

		blogContentsService.saveEditBlog(blogContents);
		mav.setViewName("/editblog_success");
		return mav;
	}

	//ユーザー一覧画面表示
	@GetMapping("/edit_alluser")
	public ModelAndView editAllUser() {

		//セッションのログインユーザ情報を取得
		EditUsers eUser = (EditUsers) session.getAttribute("loginUser");

		//セッション情報にログインユーザがない場合、ログイン画面にリダイレクト
		if(eUser == null) {
			return new ModelAndView("redirect:/login");
		}

		ModelAndView mav = new ModelAndView();
		List<EditUsers> userList = editUsersService.findAllUser();

		mav.addObject("userList", userList);
		mav.setViewName("/edit_alluser");
		return mav;
	}

	//ユーザー情報編集画面表示
	@GetMapping("/edit_profile/{id}")
	public ModelAndView editProfile(@PathVariable int id) {

		//セッションのログインユーザ情報を取得
		EditUsers eUser = (EditUsers) session.getAttribute("loginUser");

		//セッション情報にログインユーザがない場合、ログイン画面にリダイレクト
		if(eUser == null) {
			return new ModelAndView("redirect:/login");
		}

		ModelAndView mav = new ModelAndView();
		Optional<EditUsers> user = editUsersService.findBy(id);

		mav.addObject("formModel", user);
		mav.setViewName("/edit_profile");
		return mav;
	}

	//ユーザー情報編集の登録処理
	@PostMapping("/edit_profile")
	public ModelAndView editProfile(@ModelAttribute("formModel") EditUsers user) {

		ModelAndView mav = new ModelAndView();

		List<String> errorMessages = new ArrayList<String>();
		Boolean validResult = validUser(user, errorMessages, eUserValidFlg);

		if(!validResult) {
			mav.addObject("errorMessages",errorMessages);
			return mav;
		}

		//20220112 編集でパスワード入力しなかった場合、元のパスワードを登録する処理
		if(user.getPassword().isBlank()) {
			Optional<EditUsers> eu = editUsersService.findBy(user.getId());
			user.setPassword(eu.get().getPassword());

			Boolean result = editUsersService.noPassSave(user);
			if(!result) {
				mav.addObject("alert", "このメールアドレスはすでに登録されています");
				return mav;
			}

		}else {
			Boolean result = editUsersService.passSave(user);
			if(!result) {
				mav.addObject("alert", "このメールアドレスはすでに登録されています");
				return mav;
			}
		}

		mav.addObject("successMessage", "ユーザー情報の変更が完了いたしました。");
		mav.setViewName("/editprofile_success");
		return mav;
	}

	//管理ユーザー新規登録の文字数等チェック
	private Boolean validUser(EditUsers user, List<String> errorMessages, String registerFlg){
		String name = user.getName();
		String email = user.getEmail();
		String account = user.getAccount();
		String password = user.getPassword();

		//名前チェック
		if(name.length() > 20 || name.isBlank()) {
			errorMessages.add("名前を20文字以内で設定してください");
		}
		//メールアドレスチェック
		if(email.length() > 50 || email.isBlank()) {
			errorMessages.add("メールアドレスを50文字以内で設定してください");
		}
		//アカウント名チェック
		if(account.length() > 20 || account.isBlank()) {
			errorMessages.add("アカウント名を20文字以内で設定してください");
		}
		//パスワードチェック
		if(registerFlg.equals("0")) {

			//新規登録の場合、空白の場合もチェックする
			if(password.length() > 200 || password.isBlank()) {
				errorMessages.add("パスワードを200文字以内で設定してください");
			}

		}else {
			//ユーザー情報の編集の場合
			if(password.length() > 200) {
				errorMessages.add("パスワードは200文字以内で設定してください。");
			}
		}

		//エラーメッセージ該当したかどうか判定
		if(errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

	//blog新規作成の文字数等判定
	private Boolean validBlog(BlogContents blog, List<String> errorMessages) {

		String category_id = blog.getCategory_id();
		String title = blog.getTitle();
		String article = blog.getArticle();

		if(category_id.isBlank() || category_id.equals("NG")) {
			errorMessages.add("カテゴリを選択してください");
		}

		if(title.length() > 50 || title.isBlank()) {
			errorMessages.add("タイトルを50文字以内で入力してください");
		}

		if(article.length() > 1000 || article.isBlank()) {
			errorMessages.add("本文を1000文字以内で入力してください");
		}

		if(errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
