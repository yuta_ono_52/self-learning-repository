package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Report;

@Repository
public interface ReportRepository extends JpaRepository<Report, Integer>{
	//何も書かない
	//JpaRepositoryを継承していて、そちらの処理を実行するから
}
